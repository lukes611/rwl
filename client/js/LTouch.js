function LTouch(id, original_point)
{
	this.id = id;
	this.original = original_point.clone();
	this.latest = this.original.clone();
}
LTouch.prototype.update_point = function(p)
{
	this.latest = p.clone();
};

LTouch.prototype.dir = function()
{
	var m = this.latest.dist(this.original);
	if(m == 0) return new R2(0,0);
	var tmp = this.latest.clone();
	tmp.subtract(this.original);
	tmp.normalize();
	return tmp;
};

LTouch.prototype.to_string = function()
{
	return '' + this.id + ': ' + '(' + this.original.x + ',' + this.original.y + ') -> ' + '(' + this.latest.x + ',' + this.latest.y + ')';
};

function LTouchHandler()
{
	this.list = [];
}

LTouchHandler.prototype.update = function(id, x, y)
{
	
	for(var i = 0; i < this.list.length; i++)
	{
		if(this.list[i].id == id)
		{
			this.list[i].update_point(new R2(x,y));
			return;
		}
	}
	var nt = new LTouch(id, new R2(x,y));
	this.list.push(nt);
};

LTouchHandler.prototype.keep_only = function(id_list)
{
	var nl = [];
	for(var i = 0; i < this.list.length; i++)
	{
		if(id_list.indexOf(this.list[i].id) >= 0) nl.push(this.list[i]);
	}
	this.list = nl;
};

LTouchHandler.prototype.to_string = function()
{
	var rv = '';
	for(var i = 0; i < this.list.length; i++)
	{
		rv += this.list[i].to_string() + ', ';
	}
	return rv;
};

LTouchHandler.prototype.print_debug = function()
{
	if(this.list.length > 0) console.log(this.to_string());
};

LTouchHandler.prototype.start = function(html_element_id)
{
	var me = this;
	var xc = -60;
	var yc = -25;
	var wind = (typeof html_element_id == 'undefined')? document : '#' + html_element_id;
	console.log(wind);
	$(wind).on("touchstart", function(e) {
		var t = e.originalEvent.touches;
		for(var i = 0; i < t.length; i++)
		{
			me.update(t[i].identifier, t[i].pageX, t[i].pageY);
		}
		//me.print_debug();
	});
	
	$(wind).on("touchend", function(e) {
		var t = e.originalEvent.touches;
		var rlist = [];
		for(var i = 0; i < t.length; i++)
		{
			rlist.push(t[i].identifier);
		}
		me.keep_only(rlist);
		//me.print_debug();
	});
	
	$(wind).on("touchmove", function(e) {
		var t = e.originalEvent.touches;
		for(var i = 0; i < t.length; i++)
		{
			me.update(t[i].identifier, t[i].pageX, t[i].pageY);
		}
		//me.print_debug();
	});
};

