/*
	Keyboard listening object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/


//init a keyboard listener object
function LKB()
{
	this.list = [];
	this.listeners = [];
	var me = this, i;
	
	$(document).keydown(function(e){
		me.down(e.keyCode);
		for(i = 0; i < me.listeners.length; i++){
			if(me.listeners[i].type == 'down')
				if(e.keyCode == me.listeners[i].kc) me.listeners[i].f();
		}
	});
	$(document).keyup(function(e){
		me.up(e.keyCode);
	});
	
	this.char_list = 'abcdefghijklmnopqrstuvwxyz'.split('');
	this.key_code_list = [];
	var i = 0;
	for(i = 65; i <= 90; i++) this.key_code_list.push(i);
}

//set key to be down
LKB.prototype.down = function(key){
	if(this.list.indexOf(key) == -1) this.list.push(key);
};

//reset the listener
LKB.prototype.reset = function(){
	this.list = [];
};

//records a key as no longer down
LKB.prototype.up = function(key){
	var index = this.list.indexOf(key);
	if(index != -1) this.list.splice(index, 1);
};

//checks if a key is down by web ascii value
LKB.prototype.is_down = function(key){
	return this.list.indexOf(key) != -1;
};

//checks to see if a key is down by letter
LKB.prototype.letter_is_down = function(letter){
	var key = this.letter_to_keycode(letter);
	return this.list.indexOf(key) != -1;
};

//converts a letter to a keycode
LKB.prototype.letter_to_keycode = function(letter){
	var index = this.char_list.indexOf(letter);
	if(index == -1) return -1;
	return this.key_code_list[index];
};

//converts a keycode to a letter
LKB.prototype.keycode_to_letter = function(kc){
	var index = this.key_code_list.indexOf(kc);
	if(index == -1) return '' + kc;
	return this.char_list[index];
};

//checks an array of letters returning true is any of them are down
LKB.prototype.check_letters = function(letters){
	var i = 0;
	for(; i < letters.length; i++)
		if(this.letter_is_down(letters[i])) return true;
	return false;
};

//converts the current state of keys down to a string representing a movement direction for games using wasd
LKB.prototype.wasd_movement_string = function(){
	var st = '';
	if(this.letter_is_down('a'))
		st += 'left';
	else if(this.letter_is_down('d'))
		st += 'right';
	if(this.letter_is_down('w'))
		st += 'up';
	else if(this.letter_is_down('s'))
		st += 'down';
	return st;
};

//converts the current state of the keyboard to a string
LKB.prototype.to_string = function(){
	var i = 0;
	var rv = '';
	for(; i < this.list.length; i++)
		rv += this.keycode_to_letter(this.list[i]);
	return rv;
};

//adds a listener for a key when it goes down: takes in the ascii code and a callback function
LKB.prototype.listen = function(keycode, f){
	this.listeners.push({
		type : 'down',
		kc : keycode,
		f : f
	});
};

