/*
	Object Manipulator for RWL raptor, health and battery - object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//an object for manipulation of the object data
function LObjectController(object_data){
	this.data = object_data;
	this._frame = 0;
	this._timer = new LTimer(40);
}

//start timer for health spinning
LObjectController.prototype.start = function(){
	this._timer.start();
};

//get the angle for which the health item should be rotated at any point in time
LObjectController.prototype.frame = function(){
	var rv = Math.round(this._frame);
	return rv;
};

//perform a time step (updates timers and the angle for rotating the health)
LObjectController.prototype.time_step = function(t){
	for(var i = 0; i < this._timer.tick(); i++){
		this._frame += t * 20;
		this._frame %= 360;
	}
};

//a matrix for finding the place in which a laser should be initialized based on the location of the player and 
//the direction they are facing
LObjectController.prototype.matrix_l = function(rotation_angle, location){
	rotation_angle %= 360;
	var sam = 0.008;
	var rv = new LM(4,4); rv.init_scaling_matrix(-sam,sam,sam);
	var tmp = new LM(4,4); tmp.init_rotation_matrix_R3(0,rotation_angle,0,true);
	rv = rv.multiply(tmp);
	tmp.init_translation_matrix(location.x, location.y, location.z);
	rv = tmp.multiply(rv);
	return rv;
};

//gives a point to initialize a laser round given a raptors: angle and location
LObjectController.prototype.laser_round_init_location = function(rotation_angle, location){
	var m = this.matrix_l(rotation_angle, location);
	var point = this.data.guns[0].points[1];
	var p = new R3(point.x, point.y, point.z);
	m.multiply_R3(p);
	return p;
};

