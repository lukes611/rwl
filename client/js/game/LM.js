/*
	Matrix Object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

var module = module || undefined;

//constructs a matrix given number of rows and collumns
function LM(r, c){
	this.r = r;
	this.c = c;
	this.d = new Array(this.r*this.c);
}

//initializes matrix from an array of numbers and the number of rows and columns
LM.prototype.init_from_array = function(ar, r, c){
	this.r = r;
	this.c = c;
	this.d = new Array(this.r*this.c);
	var i = 0;
	for(; i < ar.length; i++) this.d[i] = ar[i];
};

//sets the elements to the elements of ar
LM.prototype.set_from_array = function(ar){
	var i = 0;
	for(; i < ar.length; i++) this.d[i] = ar[i];
};

//gets a replica of this matrix
LM.prototype.clone = function(){
	var rv = new LM(this.r, this.c);
	rv.set_from_array(this.d);
	return rv;
};

//multiplies this matrix by m2, returning another matrix
LM.prototype.multiply = function(m2){
	if(this.c != m2.r) return null;
	var rv = new LM(this.r, m2.c);
	var r, c, k, sum;
	for(r = 0; r < rv.r; r++){
		for(c = 0; c < rv.c; c++){
			sum = 0;
			for(k = 0; k < this.c; k++){
				sum += this.get(r, k) * m2.get(k, c);
			}
			rv.set(r,c, sum);
		}
	}
	return rv;
};


//sets all matrix elements to v
LM.prototype.set_all = function(v){
	for(var i = 0; i < this.d.length; i++) this.d[i] = 0;
};

//sets as the identity matrix
LM.prototype.set_identity = function(){
	for(var r = 0; r < this.r; r++)
		for(var c = 0; c < this.c; c++)
			if(r == c) this.set(r,c, 1);
			else this.set(r,c, 0);
};

//initializes a translation matrix, for R3 just call f(tx, ty, tz) for R2 call f(tx, ty)
LM.prototype.init_translation_matrix = function(){
	var num_args = arguments.length;
	this.r = num_args+1;
	this.c = num_args+1;
	this.d = new Array(this.r*this.c);
	this.set_identity();
	for(var i = 0; i < num_args; i++) this.set(i, num_args, arguments[i]);
};
//initializes a scaling matrix, for R3 just call f(tx, ty, tz) for R2 call f(tx, ty)
LM.prototype.init_scaling_matrix = function(){
	var num_args = arguments.length;
	this.r = num_args+1;
	this.c = num_args+1;
	this.d = new Array(this.r*this.c);
	this.set_identity();
	for(var i = 0; i < num_args; i++) this.set(i, i, arguments[i]);
};

//initializes a rotation matrix for R3 space (4x4), takes radians unless fourth parameter is specified
LM.prototype.init_rotation_matrix_R3 = function(rx, ry, rz, use_angles){
	var scalar = 1.0;
	if(use_angles !== undefined){
		if(use_angles == true) scalar = Math.PI / 180.0;
	}
	rx *= scalar;
	ry *= scalar;
	rz *= scalar;
	var cosx = Math.cos(rx);
	var sinx = Math.sin(rx);
	var cosy = Math.cos(ry);
	var siny = Math.sin(ry);
	var cosz = Math.cos(rz);
	var sinz = Math.sin(rz);
	var dx = [1,0,0,0,  0,cosx,-sinx,0,   0,sinx,cosx,0,    0,0,0,1];
	var dy = [cosy,0,siny,0,   0,1,0,0,   -siny,0,cosy,0,    0,0,0,1];
	var dz = [cosz,-sinz,0,0,   sinz,cosz,0,0,    0,0,1,0,    0,0,0,1];
	var mx = new LM(4,4);
	mx.set_from_array(dx);
	var my = new LM(4,4);
	my.set_from_array(dy);
	var mz = new LM(4,4);
	mz.set_from_array(dz);
	var tmp = my.multiply(mx);
	var this_ = mz.multiply(tmp);
	this.r = this_.r;
	this.c = this_.c;
	this.d = this_.d;
};

//initializes a rotation matrix for R2 space (3x3), takes radians unless fourth parameter is specified
LM.prototype.init_rotation_matrix_R2 = function(r, use_angles){
	var scalar = 1.0;
	if(use_angles !== undefined){
		if(use_angles == true) scalar = Math.PI / 180.0;
	}
	r *= scalar;
	var cosr = Math.cos(r);
	var sinr = Math.sin(r);
	var d = [cosr,-sinr,0,   sinr,cosr,0,   0,0,1];
	this.init_from_array(d, 3, 3);
};

//gets a string version of this matrix
LM.prototype.to_string = function(){
	var r, c;
	var rv = '[';
	for(r = 0; r < this.r; r++){
		rv += '[';
		for(c = 0; c < this.c; c++){
			rv += this.get(r,c);
			if(c < this.c-1) rv += ' ';
		}
		rv += ']';
	}
	return rv + ']';
};

//mutliplies p by this matrix (4x4 multiplying with [p.x p.y p.z 1]^T
LM.prototype.multiply_R3 = function(p){
	var x = p.x;
	var y = p.y;
	var z = p.z;
	p.x = x*this.get(0,0) + y*this.get(0,1) + z*this.get(0,2) + this.get(0,3);
	p.y = x*this.get(1,0) + y*this.get(1,1) + z*this.get(1,2) + this.get(1,3);
	p.z = x*this.get(2,0) + y*this.get(2,1) + z*this.get(2,2) + this.get(2,3);
};

LM.prototype.multiply_R3_round = function(p){
	var x = p.x;
	var y = p.y;
	var z = p.z;
	var d = this.d;
	p.x = Math.round(x*[0] + y*d[1] + z*d[2] + d[3]);
	p.y = Math.round(x*d[4] + y*d[5] + z*d[6] + d[7]);
	p.z = Math.round(x*d[8] + y*d[9] + z*d[10] + d[11]);
};

LM.prototype.multiply_R3_roundxy = function(p){
	var x = p.x;
	var y = p.y;
	var z = p.z;
	p.x = Math.round(x*this.d[0] + y*this.d[1] + z*this.d[2] + this.d[3]);
	p.y = Math.round(x*this.d[4] + y*this.d[5] + z*this.d[6] + this.d[7]);
};

//mutliplies p by this matrix (3x3 multiplying with [p.x p.y 1]^T
LM.prototype.multiply_R2 = function(p){
	if(this.r != 3 || this.c != 3) return;
	var x = p.x;
	var y = p.y;
	p.x = x*this.get(0,0) + y*this.get(0,1) + this.get(0,2);
	p.y = x*this.get(1,0) + y*this.get(1,1) + this.get(1,2);
	var w = x*this.get(2,0) + y*this.get(2,1) + this.get(2,2);
	p.x /= w;
	p.y /= w;
};

//initialize an isometric transform matrix
LM.prototype.init_iso_R3 = function(){
	if(this.r!=4 || this.c!=4){
		this.r = 4;
		this.c = 4;
		this.d = new Array(4,4);
	}
	this.init_rotation_matrix_R3(0, 45, 0, true);
	var tmp = new LM(4,4);
	tmp.init_rotation_matrix_R3(30, 0, 0, true);
	tmp = tmp.multiply(this);
	this.r = tmp.r;
	this.c = tmp.c;
	this.d = tmp.d;
};

//gets the value at row r, column c
LM.prototype.get = function(r, c){
	return this.d[r*this.c + c];
};

//sets the value at row r, column c to v
LM.prototype.set = function(r, c, v){
	this.d[r*this.c + c] = v;
};

if(module !== undefined) module.exports = LM;