/*
	A shooting ai plan for raptors with lasers
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/


//shooting plan: takes in current position, the direction to shoot and game g
//generates a shooting plan, first move in direction d: then shoot at least once
//checking before shooting additional times
function Shooting_Plan(player, game, dir, max_shots, visibility){
	this.max_shots = max_shots;
	this.player = player;
	this.game = game;
	this.old_pos = this.player.pos();
	this.dir = dir.clone();
	this.has_moved = false;
	this.shots_taken = 0;
	if(this.dir.dist(this.player.get_dir()) < 0.1) this.has_moved = true;
	this.visibility = visibility;
}

//returns whether this ai should shoot or not
Shooting_Plan.prototype.should_shoot = function(){
	if(!this.has_moved)
		return false;
	this.shots_taken++;
	if(this.shots_taken >= this.max_shots) return;
	var close_players = this.game.other_visible_players(this.player.name, this.visibility);
	var rv = this.game.check_aiming(this.player.name, close_players, this.visibility+1);
	if(!rv) this.shots_taken = this.max_shots;
	return rv;
};

//returns whether this ai should move or not
Shooting_Plan.prototype.should_move = function(){
	return !this.has_moved;
};

//returns the next direction the ai should move in (if any)
Shooting_Plan.prototype.next_direction = function(){
	if(this.has_moved) return new R3(0,0,0);
	this.has_moved = true;
	return this.dir.clone();
};

//perform 1 time step for this ai
Shooting_Plan.prototype.time_step = function(){};

//returns whether the plan is complete
Shooting_Plan.prototype.plan_complete = function(){
	if(this.shots_taken >= this.max_shots)
		return true;
	return false;
};

//returns whether this ai is stuck or not
Shooting_Plan.prototype.stuck = function(){
	return false;
};