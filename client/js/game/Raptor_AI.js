/*
	More complex raptor AI - Object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
ai:
	modes:
		defence [local]:
			start if: health low / low ammo and see other
			do: run opposite direction from others, get ammo / health
			end if: escaped from enemy / have more health / ammo
		offence [local]:
			start if: have ammo / health, see other player
			do: move to other player
			end if: cant see anyone, my health or ammo is low
		sniper:
			start if: can shoot someone
			do: shoot
			end if: cant see anyone
		wanderer:
			start if: nothing else
			do: move through map, get ammo / get health
			end if: see player
*/

//constructor takes the players name and the game object
function Raptor_AI(player_name, game){
	this.time = 0;
	this.visibility = RWLSETTINGS.AI_VISIBILITY_DIST + Math.round(Math.random()*4 - 2);
	this.health_scared = Math.round(Math.random() * 2);
	this.ammo_scared = Math.round(Math.random() * 4);
	this.game = game;
	this.player_name = player_name;
	this.player = this.game.get_player(this.player_name);
	this.past_positions = [this.player.pos()];
	this.plan_type = 'wanderer';
	this.load_wander_plan();
	this.check_plan_timer = new LTimer(80);
	this.execute_plan_timer = new LTimer(50);
}

//start timers for ai
Raptor_AI.prototype.start = function(){
	this.check_plan_timer.start();
	this.execute_plan_timer.start();
};

//make ai remember this location as {explored || dangerous}
Raptor_AI.prototype.add_location_memory = function(p){
	this.past_positions.push(p.clone());
	if(this.past_positions.length >= RWLSETTINGS.AI_MEMORY_LENGTH) this.past_positions.shift();
};

//returns a cost for moving to position p1 given a list of location you want to avoid in increasing worseness order
Raptor_AI.prototype.cost_function_for_evil_list = function(p1, evil_list){
	var sum = 0;
	var scalar = 0;
	for(var i = 0; i < evil_list.length; i++){
		sum += evil_list[i].dist(p1) * scalar;
		scalar += 4;
	}
	return sum;
};

//finds the optimal point to move to given a list of points and another list of points to keep away from
Raptor_AI.prototype.optimal_move_away_from = function(selection_list, evil_list)
{
	var me = this;
	selection_list.sort(function(b,a){
		return me.cost_function_for_evil_list(a, evil_list) - me.cost_function_for_evil_list(b, evil_list);
	});
	
	var until = Math.min(selection_list.length, 2);
	var index = Math.floor(Math.random() * until);
	return selection_list[index];
};

//load in the plan for the raptor to explore the area, pickning up items if need be
Raptor_AI.prototype.load_wander_plan = function(){
	var healths = this.game.visible_healths(this.player_name, this.visibility);
	if(healths.length > 0 && this.player.health < RWLSETTINGS.PLAYER_MAX_HEALTH){
		var pos = this.player.pos();
		healths.sort(function(a,b){ return a.dist(pos) - b.dist(pos);});
		var goal = healths[0];
		this.plan = new Walking_Plan(this.player, this.game, goal, this.visibility);
		return;
	}
	var batteries = this.game.visible_batteries(this.player_name, this.visibility);
	if(batteries.length > 0 && this.player.ammo < RWLSETTINGS.PLAYER_MAX_AMMO){
		var pos = this.player.pos();
		batteries.sort(function(a,b){ return a.dist(pos) - b.dist(pos);});
		var goal = batteries[0];
		this.plan = new Walking_Plan(this.player, this.game, goal, this.visibility);
		return;
	}
	var other_points = this.game.get_surrounding_safe_points(this.player_name, this.visibility);
	if(other_points.length != 0){
		var pos = this.player.pos();
		var goal = this.optimal_move_away_from(other_points, this.past_positions);
		this.plan = new Walking_Plan(this.player, this.game, goal, this.visibility);
		return;
	}
};

//load the plan returned by which_plan()
Raptor_AI.prototype.load_which_plan = function(which){
	if(which.new_type == 'wanderer'){
		this.plan_type = 'wanderer';
		this.load_wander_plan();
	}else if(which.new_type == 'sniper'){
		this.plan_type = 'sniper';
		this.plan = new Shooting_Plan(this.player, this.game, which.dir, 2, this.visibility);
	}else if(which.new_type == 'defence'){
		this.plan_type = 'defence';
		for(var i = which.players.length-1; i >= 0; i--)
			this.add_location_memory(which.players[i].clone());
		this.load_wander_plan();
	}else if(which.new_type == 'offence'){
		this.plan_type = 'offence';
		this.plan = new Walking_Plan(this.player, this.game, which.move_to, this.visibility);
	}
};

//returns a string representation fo the ai object for debugging
Raptor_AI.prototype.to_string = function(){
	return this.plan_type + '; can execute ai:  ' + (this.execute_plan_timer.can_tick()+'')[0];
};

//returns which plan the ai should be on
Raptor_AI.prototype.which_plan = function()
{
	var other_players_visible = this.game.other_visible_players(this.player_name, this.visibility, true);
	if(other_players_visible.length != 0){
		if(this.player.health <= (1+this.health_scared) || this.player.ammo <= this.ammo_scared){
			var rv = {new_type:'defence'};
			rv.players = [];
			var i = 0;
			for(i = 0; i < other_players_visible.length; i++){
				rv.players.push(other_players_visible[i].pos());
			}
			return rv;
		}
		
		//check if should return sniper
		var i = 0;
		for(; i < other_players_visible.length; i++){
			if(this.game.could_aim_at(this.player_name, other_players_visible[i].name, this.visibility)){
				var dir = other_players_visible[i].pos();
				dir.subtract(this.player.pos());
				dir.normalize();
				return {new_type:'sniper', dir : dir};
			}
		}
		return {new_type:'offence', move_to : other_players_visible[0].pos()};
	}else{
		return {new_type:'wanderer'};
	}
};


//returns whether the ai should shoot or not
Raptor_AI.prototype.should_shoot = function(){
	if(this.plan !== undefined) return this.plan.should_shoot();
	return false;
};

//returns whether the ai should move or not
Raptor_AI.prototype.should_move = function(){
	if(this.plan !== undefined) return this.plan.should_move();
	return false;
};

//returns the next point to move
Raptor_AI.prototype.next_direction = function(){
	if(this.plan !== undefined)
		return this.plan.next_direction();
	return new R3(0,0,0);
};

//clocks a single time step for the ai
Raptor_AI.prototype.time_step = function(t){
	var which = null;
	t = t || 0.2;
	var ticks = 0;
	if(this.plan !== undefined){
		var num_ticks = this.check_plan_timer.tick();
		if(num_ticks >= 1){
			which = this.which_plan();
			if(which.new_type != this.plan_type || this.plan.stuck()){
				this.load_which_plan(which);
			}
		}
		
		num_ticks = this.execute_plan_timer.tick() > 0
					?	1
					:	0;
		for(ticks = 0; ticks < num_ticks; ticks++){
			if(this.plan.plan_complete()){
				if(this.plan_type == 'wanderer'){
					this.add_location_memory(this.player.pos());
				}
				if(which == null) which = this.which_plan();
				this.load_which_plan(which);
			}
			
			if(this.should_move()){
				var dir = this.next_direction();
				this.player.request_move(dir);
			}
			if(this.should_shoot()){
				this.player.request_shoot(this.game.raptor_controller);
			}
			this.plan.time_step();
		}
		this.time += t;
	}
};




