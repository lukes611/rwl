/*
	Player object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//construct a player with a given name, id [and ai object (optional)]
function LPlayer(name, id){
	this.ai = undefined;
	this.is_ai = false;
	
	this.id = id;
	this.name = name;
	this.health = RWLSETTINGS.PLAYER_MAX_HEALTH;
	this.alive = true;
	this.revive_try = false;
	
	this.location = new R3(0,0,0);
	this.ammo = RWLSETTINGS.PLAYER_MAX_AMMO;
	
	this.angle = 0;
	this.kill_count = 0;
	this.death_count = 0;
	this._frame = 0; //note do not access this raw: contains a double rather than int
	this.charge = 0; //a variable representing the player getting shot
	this.raptor_color = new R3(0,0,0);
	this.raptor_color.init_random();
	this.raptor_color.scale(0.8); this.raptor_color.add(new R3(0.2,0.2,0.2));
	
	this.gun_color = new R3(0,0,0);
	this.gun_color.init_random();
	this.gun_color.scale(.6); this.gun_color.add(new R3(0.4,0.4,0.4));
	
	this.moves = [];
	this.fires = 0;
}

//returns a clone of the players position
LPlayer.prototype.pos = function(){
	return this.location.clone();
};

//returns a floored clone of the players position
LPlayer.prototype.posf = function(){
	var rv = this.location.clone();
	rv.floor();
	return rv;
};

//initializes player to a defined position
LPlayer.prototype.init_position = function(p){
	this.location = p.clone();
};


//collects the current animation frame
LPlayer.prototype.frame = function(){
	var rv = Math.round(this._frame);
	rv = (rv < 0) ? rv + 5 : rv;
	rv %= 5;
	return rv;
};

//increments to the next frame: based on time: takes in double representing elapsed walking time
LPlayer.prototype.inc_frame = function(t){
	t = t || 0.2;
	this._frame += t;
};

//command player to fire a laser round: returns a new Llaser:
LPlayer.prototype.request_shoot = function(){
	if(this.ammo <= 0) return;
	this.ammo -= 1;
	this.fires = 1;
};

//gets a new laser round based on the players direction and position, uses the object controller as an argument
LPlayer.prototype.get_round = function(object_controller){
	var scalar = (180/Math.PI);
	var radian_angle = this.angle / scalar;
	return new Llaser(this.name, object_controller.laser_round_init_location(this.angle, this.pos()), new R3(Math.cos(radian_angle),0,Math.sin(radian_angle)));
};

//requests a player to move in a certain direction
LPlayer.prototype.request_move = function(dir){
	this.moves = [dir];
};

//returns the raptors color: could have been shot in which case charge animates this
LPlayer.prototype.get_color = function()
{
	if(!this.alive && this.charge == 0) return undefined;
	if(this.charge % 2 == 0 || this.charge == 0) return this.raptor_color;
	if(this.health > 0) return new R3(1, 0, 0);
	return undefined;
};

//returns the raptors color: 0 - do not draw, 1 - normal color, 2 - red
LPlayer.prototype.colorType = function()
{
	if(!this.alive && this.charge == 0) return 0;
	if(this.charge % 2 == 0 || this.charge == 0) return 1;
	if(this.health > 0) return 2;
	return 0;
};


//make a player get shot
LPlayer.prototype.get_shot = function(){
	if(this.alive){
		this.charge = 10;
		this.health -= 1;
		if(this.health == 0){
			this.die();
			return true;
		}
	}
	return false;
};

//kill off the player
LPlayer.prototype.die = function(){
	this.health = 0;
	this.alive = false;
	this.charge = 10;
	this.death_count++;
	this.revive_try = true;
};

//revive the player
LPlayer.prototype.revive = function(){
	if(this.can_revive()){
		this.health = RWLSETTINGS.PLAYER_MAX_HEALTH;
		this.alive = true;
		this.ammo = RWLSETTINGS.PLAYER_MAX_AMMO;
		this.charge = 0;
		this.revive_try = false;
		
		if(this.is_ai){
			this.ai.load_wander_plan();
			this.ai.plan_type = 'wanderer';
		}
		
		return true;
	}
	return false;
};

//check if the player is allowed to revive
LPlayer.prototype.can_revive = function(){
	return !this.alive && this.charge == 0;
};

//attempt to revive player -> note changes only occur during a game time step
LPlayer.prototype.try_to_revive = function(){
	this.revive_try = true;
};


//reduce the animation charge for deaths and shots
LPlayer.prototype.reduce_charge = function(t){
	if(this.charge == 0) return;
	var new_charge = this.charge - t;
	if(new_charge < 0) this.charge = 0;
	else this.charge = Math.floor(new_charge);
};

//increment the kill count
LPlayer.prototype.score = function(){
	this.kill_count++;
};

//get a string version of the player
LPlayer.prototype.to_string = function(){
	var st = 'name: ' + this.name;
	st += '; health: ' + this.health + '; ammo: ' + this.ammo + '; kills: ' + this.kill_count + '; deaths: ' + this.death_count + ';';
	return st;
};

//get a debug string version of the player
LPlayer.prototype.to_string_debug = function(){
	var st = 'name: ' + this.name;
	st += '; health: ' + this.health + '; ammo: ' + this.ammo + '; kills: ' + this.kill_count + '; deaths: ' + this.death_count + ';';
	var p = this.pos();
	p.scale(10);
	p.round();
	p.divide(10);
	st += ' position: ' + p.to_string() + ';';
	if(this.is_ai) st += ' ' + this.ai.to_string();
	return st;
};

//reset health to max
LPlayer.prototype.heal = function(){
	this.health = RWLSETTINGS.PLAYER_MAX_HEALTH;
};

//reset ammo to max
LPlayer.prototype.charge_laser = function(){
	this.ammo = RWLSETTINGS.PLAYER_MAX_AMMO;
};

//get the direction the player is facing in R3 space
LPlayer.prototype.get_dir = function(){
	var scalar = (180/Math.PI);
	var radian_angle = this.angle / scalar;
	return new R3(Math.cos(radian_angle),0,Math.sin(radian_angle));
};