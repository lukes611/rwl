/*
	Path finding Object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//a basic state object
function LState3D(p, prev){
	this.p = p;
	this.prev = prev;
}

//returns the next states based on the current state, takes in the map object
LState3D.prototype.next_states = function(map, goal_point){
	var rv = [];
	var x = this.p.x;
	var z = this.p.z;
	//left
	if(map.in_bounds(x-1, z)) if(map.get(x-1,z) == 0) rv.push(new LState3D(new R3(x-1, 0, z), this));
	//right
	if(map.in_bounds(x+1, z)) if(map.get(x+1,z) == 0) rv.push(new LState3D(new R3(x+1, 0, z), this));
	//down
	if(map.in_bounds(x, z+1)) if(map.get(x,z+1) == 0) rv.push(new LState3D(new R3(x, 0, z+1), this));
	//up
	if(map.in_bounds(x, z-1)) if(map.get(x,z-1) == 0) rv.push(new LState3D(new R3(x, 0, z-1), this));
	
	rv.sort(function(a, b){
		return  a.p.dist(goal_point) - b.p.dist(goal_point);
	});
	
	return rv;
};

//checks for equality amongst states
LState3D.prototype.equals = function(state2){
	return this.p.x == state2.p.x && this.p.z == state2.p.z;
};

//checks if a  state is in a visited list
LState3D.contains_state = function(visited_list, state){
	for(var i = 0; i < visited_list.length; i++)
		if(visited_list[i].equals(state))
			return true;
	return false;
}

//path finding object
function LState3D_Solver(start_state, goal_state, map){
	this.visited_list = [];
	this.start_state = start_state;
	this.goal_state = goal_state;
	this.map = map;
	this.final_spot = undefined;
}

//solves for the path and returns the path locations list from start to goal, else and empty list
LState3D_Solver.prototype.solve = function(){
	var rv = [];
	var found_sollution = this._solve(this.start_state);
	if(!found_sollution) return rv;
	var final_spot = this.final_spot;
	while(final_spot !== undefined){
		var np = final_spot.p.clone();
		np.x += 0.5;
		np.z += 0.5;
		rv.unshift(np);
		final_spot = final_spot.prev;
	}
	rv.shift();
	return rv;
};

//start to solve for the path
LState3D_Solver.prototype._solve = function(s){
	this.visited_list.unshift(s);
	if(s.equals(this.goal_state)){
		this.final_spot = s;
		return true;
	}
	var next_states = s.next_states(this.map, this.goal_state.p);
	for(var i = 0; i < next_states.length; i++){
		if(!LState3D.contains_state(this.visited_list, next_states[i])){
			var rv = this._solve(next_states[i]);
			if(rv) return true;
		}
	}
	return false;
};