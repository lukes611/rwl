/*
	Laser Round object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//constructs a new laser based on a starting point and direction (both R3 objects)
function Llaser(owner_name, point, dir){
	this.owner = owner_name;
	this.location = point.clone();
	this.dir = dir.clone();
	this.live = true;
}


