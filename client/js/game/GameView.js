/*
	A view controller for a game, capable of syncing across machines - Object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/
/*
game view:
	list of players:
		position, angle, scores, deaths, gunColor, raptorColor, frame
	list of bullets:
		positions, angle
	list of objects:
		position, type, (angle - for health)
	syncing mechanism:
		keep an object which updates various data,
		can update based on information from it's inner structures
		grab data from game object
		getters and setters for data
		
	update init: map, players names, number of objects of each type, gun color, primary raptor color
	iteration update: 
		players: id, position, angle, kills, deaths, raptorcolor, health, ammo, frame
		bullets: position, angle
		objects: position, type, (angle-if health)
	
	
		
*/

function GameView(playerID, gameID){
	this.players = [];
	this.rounds = [];
	this.map = null;
	this.health = [];
	this.battery = [];
	this.gameID = gameID;
	this.playerID = playerID;
}

//insert the data from gameJSON
GameView.prototype.insertGameJSON = function(gj){
	this.map = new RWLMap(2,2,0);
	this.map.fromJSON(gj.m);
	this.players = [];
	this.rounds = [];
	this.map = null;
	this.health = [];
	this.battery = [];
	for(var i = 0; i < rv.p.length; i++){
		var p = {};
		//to do finish this...
	}
};

//get JSON game init data
GameView.prototype.gameJSON = function(game){
	var rv = {};
	rv.i = game.id;
	rv.m = game.map.toJSON();
	rv.p = [];
	for(var i = 0; i < game.players.length; i++){
		var p = game.players[i];
		var pData = {};
		pData.n = p.name;
		pData.i = p.id;
		var pos = p.pos();
		pos.scale(1000);
		pos.floor();
		pos.divide(1000);
		pData.p = [pos.x, pos.y, pos.z];
		pData.a = p.angle;
		pData.k = p.kill_count;
		pData.d = p.death_count;
		pData.h = p.health;
		var rcol = p.raptor_color.clone();
		rcol.scale(255); rcol.floor();
		pData.rc = rcol;
		pData.colType = p.colorType();
		var gcol = p.gun_color.clone();
		gcol.scale(255); gcol.floor();
		pData.gc = gcol;
		pData.f = p.frame();
		rv.p.push(pData);
	}
	rv.h = [];
	for(var i = 0; i < game.health_locations.length; i++){
		var h = game.health_locations[i].clone();
		rv.h.push([h.x, h.y, h.z]);
	}
	rv.b = [];
	for(var i = 0; i < game.battery_locations.length; i++){
		var b = game.battery_locations[i].clone();
		rv.b.push([h.x, h.y, h.z]);
	}
	rv.r = [];
	for(var i = 0; i < game.laser_rounds.rounds.length; i++){
		var l = game.laser_rounds.rounds[i];
		var loc = l.location;
		var angle = l.dir.get_dual_angles()[0];
		rv.r.push([loc.x, loc.y, loc.z, angle]);
	}
	return rv;
};






