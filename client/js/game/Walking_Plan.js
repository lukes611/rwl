/*
	a basic ai walking plan object for raptors with lasers
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//generates a walking plan, moving from p1 to p2: takes in a game object
function Walking_Plan(player, game, goal, visibility){
	this.player = player;
	this.game = game;
	this.goal = goal;
	this.path = this.game.get_path(this.player.pos(), goal);
	this.visibility = visibility;
	this.same_pos_count = 0;
	this.last_pos = this.player.pos();
}

//returns whether this ai should shoot or not
Walking_Plan.prototype.should_shoot = function(){
	var close_players = this.game.visible_players(this.player.name, this.visibility);
	return this.game.check_aiming(this.player.name, close_players, this.visibility);
};

//returns whether this ai is stuck or not
Walking_Plan.prototype.stuck = function(){
	if(this.same_pos_count > 10)
		return true;
	return false;
};

//returns whether this ai should move or not
Walking_Plan.prototype.should_move = function(){
	if(this.path.length == 0) return false;
	return true;
};

//returns the next direction the ai should move in (if any)
Walking_Plan.prototype.next_direction = function(){
	if(this.path.length == 0) return new R3(0,0,0);
	var np = this.path[0].clone();
	np.subtract(this.player.location);
	np.normalize();
	return np;
};

//perform 1 time step for this ai
Walking_Plan.prototype.time_step = function(){
	if(this.path.length == 0) return new R3(0,0,0);
	var np = this.path[0];
	if(this.last_pos.dist(this.player.pos()) < 0.05) this.same_pos_count++;
	else this.same_pos_count = 0;
	this.last_pos = this.player.pos();
	if(this.player.location.dist(np) < 0.11) this.path.shift();
};

//returns whether the plan is complete
Walking_Plan.prototype.plan_complete = function(){
	return this.path.length <= 0;
};



