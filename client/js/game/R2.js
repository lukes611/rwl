/*
	2D Vector Object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

var module = module || undefined;

//constructor for 2d vector
function R2(x, y){
	this.x = x;
	this.y = y;
}

//converts this object to a string
R2.prototype.to_string = function(){
	return this.x + ' ' + this.y;
};

//intitializes this object from a string
R2.prototype.from_string = function(s){
	var xy = s.split(" ");
	this.x = Number(xyz[0]);
	this.y = Number(xyz[1]);
};

//returns a deep copy of this object
R2.prototype.clone = function(){
	return new R2(this.x, this.y);
};

//normalize coordinates
R2.prototype.normalize = function(){
	this.divide(this.mag());
};

//vector addition
R2.prototype.add = function(v2){
	this.x += v2.x;
	this.y += v2.y;
};

//vector subtraction
R2.prototype.subtract = function(v2){
	this.x -= v2.x;
	this.y -= v2.y;
};

//computes the distance between two points
R2.prototype.dist = function(p2){
	var a = p2.x - this.x;
	var b = p2.y - this.y;
	return Math.sqrt(a*a + b*b);
};

//computes the magnitude of the vector
R2.prototype.mag = function(){
	return Math.sqrt(this.x * this.x + this.y * this.y);
};

//divides the vector by a scalar number
R2.prototype.divide = function(scalar){
	this.x /= scalar;
	this.y /= scalar;
};

//scales the vector by some scalar
R2.prototype.scale = function(scalar){
	this.x *= scalar;
	this.y *= scalar;
};

//rounds the values of the vector
R2.prototype.round = function(){
	this.x = Math.round(this.x);
	this.y = Math.round(this.y);
};

//floors the values of the vector
R2.prototype.floor = function(){
	this.x = Math.floor(this.x);
	this.y = Math.floor(this.y);
};

if(module !== undefined)
	module.exports = R2;