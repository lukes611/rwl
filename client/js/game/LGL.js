/*
	3D Graphics wrapper for three.js - Object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//construct a view with a focus player and a game object
function LGL(g, focus_player_name){
	this.game = g;
	this.focus_player_name = focus_player_name;
	
	this.w = window.innerWidth;
	this.h = window.innerHeight;
	this.renderer = new THREE.WebGLRenderer({
		antialias: true
	});
	
	this.renderer.setSize(this.w,this.h);
	document.body.appendChild(this.renderer.domElement);
	
	$(this.renderer.domElement).addClass('primaryCanvas');
	console.log(this.renderer.domElement);
	
	
	this.camera = new THREE.PerspectiveCamera(45, this.w / this.h, 0.1, 10000);
	
	this.spotLight = new THREE.SpotLight( 0xffffff );
	this.spotLight.intensity = 0.4;
	this.spotLight.position.set( 0, 20, 0 );
	this.spotLight.decay = 0.2;
	
	window.addEventListener( 'resize', onWindowResize, false );
	var self = this;
	function onWindowResize(){
		self.camera.aspect = window.innerWidth / window.innerHeight;
		self.camera.updateProjectionMatrix();
		self.renderer.setSize( window.innerWidth, window.innerHeight );
	}
	
	//objects:
	this.cube = null;
	this.laser_round = null;
	this.batteries = [];
	this.health = null;
	this.raptors = [];
	this.guns = [];
	
	this.load_data();
}

//load all data to be drawn
LGL.prototype.load_data = function(){
	var cubeGeometry = new THREE.CubeGeometry(20, 20, 20);
	var cubeMaterial = this.material(0xFF00FF);
	this.cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
	
	var laserGeometry = new THREE.CubeGeometry(0.2, 0.03, 0.03);
	var laserMaterial = this.material(0x00FF00);
	this.laser_round = new THREE.Mesh(laserGeometry, laserMaterial);
	
	//load in map:
	this.load_map();
	this.load_raptors();
	this.load_objects();
};

//load_objects - loads in the battery and health objects
LGL.prototype.load_objects = function(){
	var me = this;
	var geo2 = new THREE.Geometry();
	var redmat = this.material(0xFF0000);
	var blackmat = this.material(0x000000);
	var batterymat = this.material(0xE39F45);
	var d = this.game.object_controller.data;
	for(var i = 0; i < d.health.points.length; i++){ //load health vertices
		var point = d.health.points[i];
		geo2.vertices.push(new THREE.Vector3(point.x, point.y, point.z));
	}
	for(var i = 0; i < d.health.triangles.length; i++){
		var t = d.health.triangles[i];
		geo2.faces.push(new THREE.Face3(t.x, t.y, t.z));
	}
	geo2.computeBoundingSphere();
	geo2.computeFaceNormals();
	this.health = new THREE.Mesh(geo2, redmat);
	this.health.scale.x = 0.2;
	this.health.scale.y = 0.2;
	this.health.scale.z = 0.2;
	var mats = [blackmat, batterymat, blackmat];
	d.batteries.forEach(function(bat, bi){
		var geo1 = new THREE.Geometry();
		for(var i = 0; i < bat.points.length; i++){
			var point = bat.points[i];
			geo1.vertices.push(new THREE.Vector3(point.x, point.y, point.z));
		}
		for(var i = 0; i < bat.triangles.length; i++){
			var t = bat.triangles[i];
			geo1.faces.push(new THREE.Face3(t.x, t.y, t.z));
		}
		var mesh = new THREE.Mesh(geo1, mats[bi]);
		mesh.scale.x = 0.5;
		mesh.scale.y = 0.5;
		mesh.scale.z = 0.5;
		geo1.computeBoundingSphere();
		geo1.computeFaceNormals();
		me.batteries.push(mesh);
	});
};

//load in raptor objects
LGL.prototype.load_raptors = function(){
	var mat = this.material(0xFF00FF);
	var rs = this.game.object_controller.data;
	var scalar = 0.008;
	for(var r = 0; r < rs.raptors.length; r++){
		var geo = new THREE.Geometry();
		var rr = rs.raptors[r];
		for(var i = 0; i < rr.points.length; i++){
			var p = rr.points[i];
			geo.vertices.push(new THREE.Vector3(-p.x, p.y, p.z));
		}
		for(var i = 0; i < rr.triangles.length; i++){
			var t = rr.triangles[i];
			geo.faces.push(new THREE.Face3(t.z, t.y, t.x));
		}
		geo.computeBoundingSphere();
		geo.computeFaceNormals();
		var mesh = new THREE.Mesh(geo, mat);
		mesh.scale.x = scalar;
		mesh.scale.y = scalar;
		mesh.scale.z = scalar;
		this.raptors.push(mesh);
	}
	
	//load guns:
	for(var g = 0; g < rs.guns.length; g++){
		var geo = new THREE.Geometry();
		var gg = rs.guns[g];
		for(var i = 0; i < gg.points.length; i++){
			var p = gg.points[i];
			geo.vertices.push(new THREE.Vector3(-p.x, p.y, p.z));
		}
		for(var i = 0; i < gg.triangles.length; i++){
			var t = gg.triangles[i];
			geo.faces.push(new THREE.Face3(t.z, t.y, t.x));
		}
		geo.computeBoundingSphere();
		geo.computeFaceNormals();
		var mesh = new THREE.Mesh(geo, mat);
		mesh.scale.x = scalar;
		mesh.scale.y = scalar;
		mesh.scale.z = scalar;
		this.guns.push(mesh);
	}
};

//load in map list
LGL.prototype.load_map = function(){
	var map_geo = new THREE.Geometry();
	var map = this.game.map;
	var mat = this.material(0x808080);
	this.map_cubes = [];
	this.cube_outlines = [];
	
	for(var y = 0; y < map.h; y++){
		for(var x = 0; x < map.w; x++){
			if(map.get(x,y) == 0){
				var cg = new THREE.CubeGeometry(1, 1, 1);
				var cube = new THREE.Mesh(cg, mat);
				cube.position.x = x;
				cube.position.z = y;
				cube.position.y = -1;
				var outline = new THREE.BoxHelper( cube );
				outline.material.color.set( 0x000000 );
				this.cube_outlines.push(outline);
				this.map_cubes.push(cube);
				
			}else{
				var cg = new THREE.CubeGeometry(1, 1, 1);
				var cube = new THREE.Mesh(cg, mat);
				cube.position.x = x;
				cube.position.z = y;
				cube.position.y = 0.01;
				this.map_cubes.push(cube);
				var outline = new THREE.BoxHelper( cube );
				outline.material.color.set( 0x000000 );
				this.cube_outlines.push(outline);
			}
		}
	}
};

//gets a material object based on color: format: 0xFFA812 (int)
LGL.prototype.material = function(color){
	return new THREE.MeshPhongMaterial({
					color: color,
					shininess: 10,
					specular: 0x111111,
					shading: THREE.SmoothShading
				});
};

//set the camera point and the lighting
LGL.prototype.init_cam_and_light = function(focus_point){
	this.camera.position.x = focus_point.x+2.5;
	this.camera.position.y = focus_point.y+4;
	this.camera.position.z = focus_point.z+2.5;
	
	this.spotLight.position.set( focus_point.x, 10, focus_point.z );
	this.spotLight.target.position.x = focus_point.x;
	this.spotLight.target.position.y = focus_point.y;
	this.spotLight.target.position.z = focus_point.z;
	this.spotLight.target.updateMatrixWorld();
	
	this.camera.lookAt(new THREE.Vector3(focus_point.x,focus_point.y,focus_point.z-0.5));
};

//draw a laser round
LGL.prototype.draw_laser_round = function(l, scene){
	var laser = this.laser_round.clone();
	laser.position.x += l.location.x - 0.5;
	laser.position.y += l.location.y - 0.1;
	laser.position.z += l.location.z - 0.5;
	laser.rotation.y = (-l.dir.get_dual_angles()[0]) / (180/Math.PI);
	scene.add(laser);
};

//draw a raptor
LGL.prototype.draw_player = function(player, scene){
	var c = player.get_color();
	if(c === undefined) return;
	
	var player_color = new THREE.Color(c.x, c.y, c.z);
	c = player.gun_color;
	var gun_color = new THREE.Color(c.x, c.y, c.z);
	
	var pos = player.pos();
	var r = this.raptors[player.frame()].clone();
	var g = this.guns[player.frame()].clone();
	var nmat = this.material(player_color);
	var nmatg = this.material(gun_color);
	
	
	r.material = nmat;
	g.material = nmatg;
	r.position.x = pos.x - 0.5;
	r.position.y = -0.1;
	r.position.z = pos.z - 0.5;
	g.position.x = pos.x - 0.5;
	g.position.y = -0.1;
	g.position.z = pos.z - 0.5;
	var a = -player.angle;
	r.rotation.y = (a) / (180 / Math.PI);
	g.rotation.y = (a) / (180 / Math.PI);
	scene.add(r);
	scene.add(g);
};

//draw a battery
LGL.prototype.draw_battery = function(pos, scene){
	for(var i = 0; i < 3; i++){
		var b = this.batteries[i].clone();
		b.position.x = pos.x - 0.5;
		b.position.z = pos.z - 0.5;
		b.position.y = 0.0;
		scene.add(b);
	}
};

//draw health pack
LGL.prototype.draw_health = function(pos, scene, angle){
	var h = this.health;
	h.position.x = pos.x - 0.5;
	h.position.z = pos.z - 0.5;
	h.position.y = 0.0;
	h.rotation.y = angle / (180 / Math.PI);
	scene.add(h);
};

//draw view port for game
LGL.prototype.draw_graphics = function(){
	var focus_player = this.game.get_player(this.focus_player_name);
	if(focus_player === undefined){
		console.log('no focal point');
		return;
	}
	var pos = focus_player.pos();
	
	this.init_cam_and_light(pos);
	
	var scene = new THREE.Scene;
	
	for(var i = 0; i < this.map_cubes.length; i++){
		scene.add(this.map_cubes[i]);
		//scene.add(this.cube_outlines[i]);
	}
	
	for(var i = 0; i < this.game.players.length; i++)
		this.draw_player(this.game.players[i], scene);
	
	for(var i = 0; i < this.game.laser_rounds.rounds.length; i++)
		this.draw_laser_round(this.game.laser_rounds.rounds[i], scene);
	
	for(var i = 0; i < this.game.battery_locations.length; i++)
		this.draw_battery(this.game.battery_locations[i], scene);
	
	for(var i = 0; i < this.game.health_locations.length; i++)
		this.draw_health(this.game.health_locations[i], scene, this.game.object_controller.frame());
	
	scene.add(this.spotLight);
	
	this.renderer.render(scene, this.camera);
};