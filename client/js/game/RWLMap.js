/*
	Gameplay Map object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//initialize map with width, height and a color and ratio of non-height blocks to height blocks
function RWLMap(width, height, ratio){
	this.w = width;
	this.h = height;
	this.d = new Array(this.w * this.h);
	for(var i = 0; i < this.d.length; i++){
		this.d[i] = 0;
	}
	ratio = ratio || 0.8;
	this.init_random_heights(ratio);
}

//initializes a random map, with random colors given a width and height
RWLMap.prototype.init_random_heights = function(ratio){
	for(var i = 0; i < this.d.length; i++)
		this.d[i] = (Math.random() >= ratio) ? 1 : 0;
};

//checks and executes if p_ can move in direction: dir by dir.mag() and is more than dist from a wall
//(basically: can I walk to p_ + dir, if so: move there)
RWLMap.prototype.perform_safe_move = function(p_, dir, dist){
	var p = p_.clone();
	p.add(dir);
	if(this.is_safe_position(p, dist)){
		p_.x = p.x;
		p_.y = p.y;
		p_.z = p.z;
		return true;
	}
	return false;
};

//checks if p_ can move in direction: dir by dir.mag() and is more than dist from a wall
//(basically: can I walk to p_ + dir)
RWLMap.prototype.is_safe_move = function(p_, dir, dist){
	var p = p_.clone();
	p.add(dir);
	if(this.is_safe_position(p, dist))
		return true;
	return false;
};

//computes the 2d distance between two points (x1,y1) and (x2,y2)
RWLMap.prototype.fast_dist = function(x1, y1, x2, y2){
	var a = x1-x2;
	var b = y1-y2;
	return Math.sqrt(a*a + b*b);
};


//fast version of is_safe_move but with only a point coming in, hint: use 0.8 for player
RWLMap.prototype.is_safe_position = function(p, dist){
	var x = Math.floor(p.x), a, b;
	var z = Math.floor(p.z);
	var px = p.x;
	var pz = p.z;
	
	//check at pos
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//right
	x += 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//top right
	z -= 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//top
	x -= 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//top left
	x -= 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//left
	z += 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	//bottom left
	z += 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//bottom
	x += 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	//bottom right
	x += 1;
	if(this.in_bounds(x, z)) if(this.get(x, z) > 0){
		a = (x+0.5)-px;
		b = (z+0.5)-pz;
		if(Math.sqrt(a*a + b*b) < dist) return false;
	}
	
	return true;
};

//gets the map block given the x,y location
RWLMap.prototype.get = function(x,y){
	return this.d[y*this.w + x];
};

//gets the point at the center of the map in R3 space
RWLMap.prototype.map_center = function(){
	var rv = new R3(this.w, 0, this.h);
	rv.scale(0.5);
	return rv;
};

//sets or modifies the point at x,y to v
RWLMap.prototype.set = function(x,y, v){
	this.d[y*this.w + x] = v;
};


//checks if x,y is within the 2d bounds of the map (from birds-eye view)
RWLMap.prototype.in_bounds = function(x,y){
	if(x >= 0 && y >= 0 && x < this.w && y < this.h) return true;
	return false;
};


//this sets a wall around the border of the map
RWLMap.prototype.block_off_borders = function(){
	var i = 0;
	for(i = 0; i < this.w; i++){
		this.set(i,0, 1);
		this.set(i,this.h-1, 1);
	}
	for(i = 0; i < this.h; i++){
		this.set(0, i, 1);
		this.set(this.w-1, i, 1);
	}
};

//returns a random location where a player or item can be initilized
RWLMap.prototype.get_random_location = function(){
	var x = Math.round(Math.random() * this.w);
	var y = Math.round(Math.random() * this.w);
	if(this.in_bounds(x,y)){
		var new_pos = new R3(x+0.5, 0, y+0.5);
		if(this.get(x,y) < 1) return new_pos;
	}
	return this.get_random_location();
};


//to json
RWLMap.prototype.toJSON = function(){
	var narr = [];
	for(var i = 0; i < this.d.length; i++) narr.push(this.d[i]);
	return {
		w : this.w,
		h : this.h,
		d : narr
	};
};


//from json
RWLMap.prototype.fromJSON = function(data){
	this.w = data.w;
	this.h = data.h;
	var d = [];
	for(var i = 0; i < data.d.length; i++) d.push(data.d[i]);
	this.d = d;
};
