var RWLSETTINGS = {
	MAP_WIDTH : 20,
	MAP_LENGTH : 20,
	NUM_ITEMS : 5,
	PLAYER_ITEM_SIZE : 0.8, //used for collision detection between players
	AI_WALK_INTERPOLATION_AM : 0.1, //used to interpolate for when ai checks if path is walkable
	AI_SHOOT_INTERPOLATION_AM : 0.2, //used to interpolate for when ai checks if can shoot any other players
	AI_VISIBILITY_DIST : 8, //determines an AI's visibility
	AI_MEMORY_LENGTH : 8,
	
	//player settings:
	PLAYER_MAX_HEALTH : 6,
	PLAYER_MAX_AMMO : 15,
	LASER_SPEED_PER_30_MS : 0.1
};