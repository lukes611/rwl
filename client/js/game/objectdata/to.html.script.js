/*
	Conversion code for raptors and other objects to html json for rendering RWL Game... Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

var ol = require('./obj.reader');
var fs = require('fs');


function load(name, cb){
	ol.load(name + '.obj', function(d){
		if(d.error){
			cb(undefined);
			return;
		}
		delete d.error;
		delete d.type;
		cb(d);
	});
}

function load_list(list, cb){
	var cp = [];
	cp.push.apply(cp, list);
	console.log(cp.join(' '));
	var op = [];
	var add_to_list = undefined;
	f = function(){
		if(cp.length==0){
			cb(op);
		}else{
			var item = cp.shift();
			load(item, add_to_list);
		}
	};
	var add_to_list = function(data){
		op.push(data);
		f();
	};
	f();
}

console.log('converting objects to html js.');
var list_of_objects = 'battery0,battery1,battery2,health'.split(',');
var raptors = 'rb0,rb1,rb2,rb3,rb4'.split(',');
var guns = 'rg0,rg1,rg2,rg3,rg4'.split(',');


load_list(list_of_objects, function(oblist){
	var o = oblist;
	load_list(raptors, function(rl){
		var r = rl;
		load_list(guns, function(gl){
			var g = gl;
			var ob = {
				raptors 	: r,
				guns 		: g,
				batteries	: [o[0], o[1], o[2]],
				health		: o[3]
			};
			fs.writeFileSync('object.data.js', 'var OBJ_DATA = ' + JSON.stringify(ob) + ';');
		});
	});
});
