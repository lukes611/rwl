/*
	Laser Round Database object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//constructor
function LLaserDB(){
	this.rounds = [];
}

//add a round to the db
LLaserDB.prototype.add = function(l){
	this.rounds.push(l);
};

//remove rounds which are not live
LLaserDB.prototype.remove_dead_rounds = function(){
	var narr = [];
	for(var i = 0; i < this.rounds.length; i++) if(this.rounds[i].live) narr.push(this.rounds[i]);
	this.rounds = narr;
};
