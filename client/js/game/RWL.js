/*
	RWL-Game object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//constructor takes in the raw object data
function RWL(object_data){
	this.id = 0;
	this.object_controller = new LObjectController(object_data);
	this.players = [];
	this.laser_rounds = new LLaserDB();
	this.map = new RWLMap(RWLSETTINGS.MAP_WIDTH, RWLSETTINGS.MAP_LENGTH);
	this.map.block_off_borders();
	
	this.battery_locations = [];
	this.health_locations = [];
	
	//setup a certain amount of batteries
	var num_batteries = RWLSETTINGS.NUM_ITEMS;
	var num_healths = RWLSETTINGS.NUM_ITEMS;
	var i = 0;
	for(i = 0; i < num_batteries; i++) this.battery_locations.push(this.get_random_unoccupied_position());
	for(i = 0; i < num_healths; i++) this.health_locations.push(this.get_random_unoccupied_position());;
	
	this._time = 0;
	this.laser_rounds_timer = new LTimer(10);
	this.players_timer = new LTimer(40);
	this.shooting_timer = new LTimer(200);
	
	this.id_counter = 0;
	
	
}

//start the game (initialize timers)
RWL.prototype.start = function(){
	this.object_controller.start();
	this.laser_rounds_timer.start();
	this.shooting_timer.start();
	this.players_timer.start();
	for(var i = 0; i < this.players.length; i++)
		if(this.players[i].is_ai) this.players[i].ai.start();
};

//add new player (by player object
RWL.prototype.add_player = function(player_name, is_ai){
	is_ai = is_ai || false;
	var p = new LPlayer(player_name, this.id_counter++);
	this.players.push(p);
	p.init_position(this.get_random_unoccupied_position());
	
	//initialize ai if need be
	if(is_ai){
		p.ai = new Raptor_AI(player_name, this);
		p.is_ai = true;
	}
};

//get a player based on their name
RWL.prototype.get_player = function(name){
	for(var i = 0; i < this.players.length; i++)
		if(this.players[i].name == name)
			return this.players[i];
	return undefined;
};

//returns a player based on their unique id
RWL.prototype.player_from_id = function(id){
	for(var i = 0; i < this.players.length; i++)
		if(this.players[i].id == id)
			return this.players[i];
	return undefined;
};

//request a player shoot
RWL.prototype.shoot = function(player_name){
	var p = this.get_player(player_name);
	if(p !== undefined){
		var new_laser_round = p.get_round(this.object_controller);
		if(typeof new_laser_round !== undefined){
			this.laser_rounds.add(new_laser_round);
			return true;
		}
	}
	return false;
};

//make a player get shot
RWL.prototype.get_shot = function(player_name){
	var p = this.get_player(player_name);
	if(typeof p !== undefined){
		p.get_shot();
	}
};

//make a player get killed
RWL.prototype.kill = function(player_name){
	var p = this.get_player(player_name);
	if(typeof p !== undefined){
		p.die();
		p.try_to_revive();
	}
};

//game time step
RWL.prototype.time_step = function(t){
	var i, j, ticks;
	this.object_controller.time_step(t);
	//time step for laser rounds
	var laser_ticks = this.laser_rounds_timer.tick();
	for(ticks = 0; ticks < laser_ticks; ticks++){
		for(i = 0; i < this.laser_rounds.rounds.length; i++){
			var r = this.laser_rounds.rounds[i];
			if(!r.live) continue;
			for(j = 0; j < this.players.length; j++){
				if(this.players[j].alive && r.owner != this.players[j].name){
					if(r.location.dist(this.players[j].location) < RWLSETTINGS.PLAYER_ITEM_SIZE){
						var killed_player = this.players[j].get_shot();
						var p2 = this.get_player(r.owner);
						if(typeof p2 != 'undefined' && killed_player) p2.score();
						r.live = false;
						break;
					}
				}
			}
			if(!r.live) continue;
			var change = r.dir.clone();
			change.scale(RWLSETTINGS.LASER_SPEED_PER_30_MS);
			if(!this.map.perform_safe_move(r.location, change, RWLSETTINGS.PLAYER_ITEM_SIZE)) r.live = false;
			
		}
		this.laser_rounds.remove_dead_rounds();
	}
	
	
	//time step for players:
	var player_ticks = this.players_timer.tick();
	for(ticks = 0; ticks < player_ticks; ticks++){
		for(i = 0; i < this.players.length; i++){
			if(this.players[i].revive_try){
				if(this.players[i].can_revive()){
					this.players[i].init_position(this.get_random_unoccupied_position());
					this.players[i].revive();
				}
			}
			//check if player ran into battery
			this.players[i].reduce_charge(t);
			var j, narr = [];
			for(j = 0; j < this.battery_locations.length; j++){
				if(this.battery_locations[j].dist(this.players[i].location) < RWLSETTINGS.PLAYER_ITEM_SIZE){
					this.players[i].charge_laser();
					narr.push(this.get_random_unoccupied_position());
				}else narr.push(this.battery_locations[j]);
			}
			this.battery_locations = narr;
			
			//check if player ran into health
			var j, narr = [];
			for(j = 0; j < this.health_locations.length; j++){
				if(this.health_locations[j].dist(this.players[i].location) < RWLSETTINGS.PLAYER_ITEM_SIZE){
					this.players[i].heal();
					narr.push(this.get_random_unoccupied_position());
				}else narr.push(this.health_locations[j]);
			}
			this.health_locations = narr;
			
			if(this.players[i].alive){
				if(this.players[i].moves.length != 0) this.move_player(this.players[i].name, this.players[i].moves.shift());
			}
		}
	}
	for(var i = 0; i < this.players.length; i++){
		if(this.players[i].is_ai){
			this.players[i].ai.time_step(t);
		}
	}
	
	for(var tick = 0; tick < this.shooting_timer.tick(); tick++){
		for(var i = 0; i < this.players.length; i++){
			if(this.players[i].fires > 0){
				this.shoot(this.players[i].name);
				this.players[i].fires--;
			}
		}
	}
	
	this._time += t;
};

//returns an unoccupied position on the map
RWL.prototype.get_random_unoccupied_position = function(){
	var r = this.map.get_random_location();
	var i = 0;
	for(i = 0; i < this.players.length; i++)
		if(this.players[i].location.dist(r) < RWLSETTINGS.PLAYER_ITEM_SIZE)
			return this.get_random_unoccupied_position();
	
	for(i = 0; i < this.battery_locations.length; i++)
		if(this.battery_locations[i].dist(r) < RWLSETTINGS.PLAYER_ITEM_SIZE)
			return this.get_random_unoccupied_position();
	for(i = 0; i < this.health_locations.length; i++)
		if(this.health_locations[i].dist(r) < RWLSETTINGS.PLAYER_ITEM_SIZE)
			return this.get_random_unoccupied_position();
	return r;
};

//moves a player given a player name and a vector in R3
RWL.prototype.move_player = function(player_name, new_dir_in){
	var p = this.get_player(player_name);
	if(p !== undefined){
		if(p.alive){
			var new_dir = new_dir_in.clone();
			new_dir.normalize();
			var angle_t = new_dir.get_dual_angles()[0];
			p.angle = (angle_t < 0) ? 360 + angle_t : angle_t;
			p.inc_frame(0.5);
			new_dir.scale(0.2);
			if(this.map.is_safe_move(p.location, new_dir, RWLSETTINGS.PLAYER_ITEM_SIZE)){
				var i = 0;
				var next_point = p.pos();
				next_point.add(new_dir);
				for(; i < this.players.length; i++){
					if(this.players[i].name == p.name || !this.players[i].alive)
						continue;
					if(this.players[i].location.dist(next_point) < RWLSETTINGS.PLAYER_ITEM_SIZE){
						return false;
					}
				}
				p.location = next_point;
				return true;
			}
		}
		return false;
	}
};


//more code for RWL object

//finds a path from point p1 to point p2 (both in R3)
RWL.prototype.get_basic_path = function(p1_in, p2_in){
	var p1 = p1_in.clone();
	var p2 = p2_in.clone();
	p1.floor(); p2.floor();
	
	var start = new LState3D(p1.clone());
	var goal = new LState3D(p2.clone());
	
	var solver = new LState3D_Solver(start, goal, this.map);
	return solver.solve();
};


//takes in two points and interpolates between them: returning true if a player could walk between those two points without an issue
RWL.prototype.safe_to_walk = function(pa, pb, am){
	am = am || RWLSETTINGS.AI_WALK_INTERPOLATION_AM;
	var p = pa.clone();
	var dir = pb.clone(); dir.subtract(pa); dir.normalize();
	dir.scale(am);
	while(p.dist(pb) > am){
		if(!this.map.is_safe_position(p, RWLSETTINGS.PLAYER_ITEM_SIZE))
			return false;
		p.add(dir);
	}
	return true;
};

//returns true if a player p1's shots would hit a list of players
RWL.prototype.check_aiming = function(player_name, player_list, max_dist){
	var p = this.get_player(player_name);
	if(p === undefined) return false;
	
	var am = RWLSETTINGS.AI_SHOOT_INTERPOLATION_AM, i;
	var pos = this.object_controller.laser_round_init_location(p.angle, p.pos());
	var orig = pos.clone();
	var dir = p.get_dir();
	dir.scale(am);
	//while(this.map.is_safe_move(pos, undefined, 0.5))
	while(this.map.is_safe_position(pos, RWLSETTINGS.PLAYER_ITEM_SIZE)){
		for(i = 0; i < player_list.length; i++){
			if(player_list[i].name == player_name)
				continue;
			if(player_list[i].pos().dist(pos) <= RWLSETTINGS.PLAYER_ITEM_SIZE)
				return true;
		}
		pos.add(dir);
		if(orig.dist(pos) > max_dist) 
			return false;
	}
	return false;
};


//returns true if a player p1's could turn and shoot another player:
//if yes: returns true... else: returns false
RWL.prototype.could_aim_at = function(player_name, other_player_name, max_dist){
	var am = RWLSETTINGS.AI_SHOOT_INTERPOLATION_AM, i;
	var p = this.get_player(player_name);
	if(p === undefined) return false;
	var p2 = this.get_player(other_player_name);
	if(p2 === undefined) return false;
	if(!p2.alive) return false;
	
	var pos = p.pos();
	var pos2 = p2.pos();
	var orig = pos.clone();
	var dir = pos2.clone(); dir.subtract(pos); dir.normalize();
	dir.scale(am);
	while(this.map.is_safe_position(pos, RWLSETTINGS.PLAYER_ITEM_SIZE)){
		if(pos2.dist(pos) <= RWLSETTINGS.PLAYER_ITEM_SIZE)
			return true;
		pos.add(dir);
		if(orig.dist(pos) > max_dist)
			return false;
	}
	return false;
};

//returns a list of coordinates for walking between p1 and p2
RWL.prototype.get_path = function(p1, p2){
	var path = this.get_basic_path(p1, p2);
	var me = this;
	function remove_from_chain(){
		var i = 1;
		var to_rem = -1;
		for(i = 1; i < path.length-1; i++){
			if(me.safe_to_walk(path[i-1], path[i+1])){
				to_rem = i;
				break;
			}
		}
		if(to_rem == -1) return false;
		path.splice(to_rem, 1);
		return true;
	};
	
	while(remove_from_chain()) ;
	return path;
};

//returns a list of other players visible to the input player
RWL.prototype.visible_players = function(player_name, dist){
	var rv = [];
	var i;
	var p = this.get_player(player_name);
	if(p === undefined) return rv;
	for(i = 0; i < this.players.length; i++){
		if(this.players[i].name == player_name) continue;
		if(!this.players[i].alive) continue;
		if(p.pos().dist(this.players[i].pos()) <  dist)
			rv.push(this.players[i]);
	}
	return rv;
};

//gets a list of battery packs visible to a player
RWL.prototype.visible_batteries = function(player_name, dist){
	var rv = [];
	var i;
	var p = this.get_player(player_name);
	if(p === undefined) return rv;
	var pos = p.pos();
	for(i = 0; i < this.battery_locations.length; i++){
		if(pos.dist(this.battery_locations[i]) <  dist)
			rv.push(this.battery_locations[i].clone());
	}
	return rv;
};

//gets a list of health packs visible to the player
RWL.prototype.visible_healths = function(player_name, dist){
	var rv = [];
	var i;
	var p = this.get_player(player_name);
	if(p ==- undefined) return rv;
	var pos = p.pos();
	for(i = 0; i < this.health_locations.length; i++){
		if(pos.dist(this.health_locations[i]) <  dist)
			rv.push(this.health_locations[i].clone());
	}
	return rv;
};

//gets a surrounding point in which a player: player_name can travel to: ie no other players are there
RWL.prototype.get_surrounding_safe_points = function(player_name, dist){
	var rv = [];
	var i, x, y;
	var p = this.get_player(player_name);
	if(p === undefined) return rv;
	
	var pos = p.posf();
	
	
	var yf = pos.z - dist;
	var yt = pos.z + dist;
	var xf = pos.x - dist;
	var xt = pos.x + dist;
	var others = this.other_visible_players(player_name, dist);
	for(y = yf; y <= yt; y++){
		for(x = xf; x < xt; x++){
			pos.x = x + 0.5;
			pos.z = y + 0.5;
			if(this.map.in_bounds(x, y)){
				if(this.map.get(x,y) != 0) continue;
				var safe = true;
				for(i = 0; i < others.length; i++){
					if(others[i].location.dist(pos) < RWLSETTINGS.PLAYER_ITEM_SIZE){
						safe = false;
						break;
					}
				}
				if(safe) rv.push(pos.clone());
			}
		}
	}
	return rv;
};


//returns a list of alive players who are not the player specified
RWL.prototype.other_players = function(player_name, sort_by_distance){
	sort_by_distance = sort_by_distance || false;
	var rv = [];
	var i;
	for(i = 0; i < this.players.length; i++){
		if(this.players[i].alive && this.players[i].name != player_name)
			rv.push(this.players[i]);
	}
	if(sort_by_distance){
		var p = this.get_player(player_name);
		var point = p.pos();
		rv.sort(function(a,b){
			return a.location.dist(point) - b.location.dist(point);
		});
	}
	return rv;
}

//returns a list of alive players who are not the player specified
RWL.prototype.other_visible_players = function(player_name, dist, sort_by_distance){
	sort_by_distance = sort_by_distance || false;
	var rv = [];
	var p = this.get_player(player_name);
	var point = p.pos();
	var i;
	for(i = 0; i < this.players.length; i++){
		if(this.players[i].alive && this.players[i].name != player_name && point.dist(this.players[i].location) < dist) rv.push(this.players[i]);
	}
	if(sort_by_distance){
		rv.sort(function(a,b){
			return a.location.dist(point) - b.location.dist(point);
		});
	}
	return rv;
}

