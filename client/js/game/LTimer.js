/*
	Timer object Code
	Written by: Luke Lincoln, 2015
	email: lukes611@gmail.com
	language: JavaScript
*/

//constructor takes the alarm time in milliseconds
function LTimer(alarm){
	this._start = undefined;
	this._alarm = alarm;
	this._remainder = 0;
	this.diff = 0;
}

//start the timer, use this function for resetting as well
LTimer.prototype.start = function(){
	this._start = (new Date()).getTime() + this._remainder;
	this._remainder = 0;
};

//returns the number of ticks the timer has gone through
LTimer.prototype.tick = function(){
	var end = (new Date()).getTime();
	var time_diff = (end - this._start) + this._remainder;
	if(time_diff >= this._alarm){
		this.diff = time_diff;
		var num_times = Math.floor(time_diff / this._alarm);
		this._remainder = time_diff - (num_times*this._alarm);
		this.start();
		return num_times;
	}
	return 0;
};

//returns whether a timer has moved past the first tick
LTimer.prototype.can_tick = function(){
	var end = (new Date()).getTime();
	var time_diff = (end - this._start) + this._remainder;
	if(time_diff >= this._alarm)
		return true;
	return false;
};